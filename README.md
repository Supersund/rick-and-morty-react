## My fantastic plastatastic Rick & Morty character overview

Take a trip into the fantasy world. Curious about the components used in this app? Weird. Well, your choice.

CharacterList - The flesh and bone of this majestic creature. It makes the API calls and contains the structuring of the page.

CharacterCard - Your good 'ol, non-fancy, down-to-earth Component that receives a character-object and shows the name and picture of it. It even allows you to click it to see more information about the character. Ain't that something eh?

SearchBar - Want to know if there exists a character that contains the name "poop"? Well, the SearchBar is there for you! This special search bar comes with a fully functional button! Rad!

CharacterPage - You clicked a CharacterCard, didn't you? Well, lucky for you I implemented a CharacterPage for it. Lucky dog!
