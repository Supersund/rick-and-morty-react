import React from "react";
// Import the CSS file as a module.
import styles from "./CharacterList.css";
import SearchBar from "../searchBar/SearchBar";
import CharacterCard from "../characterCard/CharacterCard";
import InfiniteScroll from "react-infinite-scroller";

// Constant To store API url.
var apiUrl = "https://rickandmortyapi.com/api/character/";

class CharacterList extends React.Component {
  // Initialize the State in Class Component.
  state = {
    characters: [],
    hasMoreItems: true,
    next: apiUrl
  };

  // Use ASYNC/AWAIT inside lifecycle method.

  async search(searchString) {
    try {
      const response = await fetch(
        apiUrl + "?name=" + searchString
      ).then(resp => resp.json());

      let chars = [];
      // Add the results from the API response.
      if (response.results) {
        chars.push(...response.results);
      }
      if (response.info.next) {
        this.setState({
          characters: chars,
          next: response.info.next,
          hasMoreItems: true
        });
      } else {
        this.setState({
          characters: chars,
          next: "",
          hasMoreItems: false
        });
      }
      console.log(this.state.characters);
      console.log(searchString);
      // ALWAYS use this.setState() in a Class Method.
    } catch (e) {
      //console.error(e);
      this.setState({
        characters: [],
        next: "",
        hasMoreItems: false
      });
    }
  }

  async loadItems(_) {
    let chars = [...this.state.characters];

    try {
      const response = await fetch(this.state.next).then(resp => resp.json());
      if (response.results) {
        chars.push(...response.results);
      }
      if (response.info.next) {
        this.setState({
          characters: chars,
          next: response.info.next
        });
      } else {
        this.setState({
          characters: chars,
          next: "",
          hasMoreItems: false
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  // Required render() method in Class Component.
  render() {
    const loader = (
      <div className="loader" key="myLoader">
        Loading ...
      </div>
    );
    // Create an array of JSX to render
    const characters = this.state.characters.map(character => {
      // This should render Character components. - Remember the key.
      return (
        <CharacterCard
          character={character}
          callback={arg => this.openCard(arg)}
          key={character.id}
        />
      );
    });

    // Render MUST return valid JSX.
    return (
      <div>
        <SearchBar callback={arg => this.search(arg)} />
        <div className={styles.CharacterList}>
          <InfiniteScroll
            loadMore={this.loadItems.bind(this)}
            hasMore={this.state.hasMoreItems}
            loader={loader}
          >
            <div className="Container">{characters}</div>
          </InfiniteScroll>
        </div>
      </div>
    );
  }
}

export default CharacterList;
