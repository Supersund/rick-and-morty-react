import React, { useState, useEffect } from "react";
import Card from "react-bootstrap/Card";

const CharacterPage = function(props) {
  const [character, setCharacter] = useState({});
  const { id } = props.location;
  const apiUrl = "https://rickandmortyapi.com/api/character/" + id;
  useEffect(() => {
    async function getData() {
      const response = await fetch(apiUrl).then(resp => resp.json());
      setCharacter(response);
      console.log(response);
    }
    getData();
  }, []);

  return (
    <div>
      <Card style={{ width: "18rem", margin: "3rem" }}>
        <Card.Img varian="top" src={character.image} alt={character.name} />
        <Card.Title>{character.name}</Card.Title>
        <Card.Body>
          <p>status: {character.status}</p>
          <p> species: {character.species}</p>
        </Card.Body>
      </Card>
    </div>
  );
};

export default CharacterPage;
