import React, { useState } from "react";

const SearchBar = function(props) {
  const [input, setInput] = useState("");
  const { callback } = props;

  function handleChange(event) {
    setInput(event.target.value);
  }
  return (
    <div className="search">
      <input
        id="search-field"
        placeholder="Search.."
        onChange={handleChange}
        onKeyDown={event => {
          if (event.keyCode === 13) callback(input);
        }}
      />
      <button
        onClick={function() {
          callback(input);
        }}
      >
        Search
      </button>
      <button onClick={() => callback("")}>Reset</button>
    </div>
  );
};

export default SearchBar;
