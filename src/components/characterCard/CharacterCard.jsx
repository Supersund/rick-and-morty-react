import React from "react";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";

const CharacterCard = function(props) {
  const { character } = props;

  return (
    <div style={{ maxWidth: "12rem", marginTop: "1rem" }}>
      <Link to={{ pathname: "/character", id: character.id }}>
        <Card>
          <Card.Img varian="top" src={character.image} alt={character.name} />
          <Card.Title>{character.name}</Card.Title>
        </Card>
      </Link>
    </div>
  );
};

export default CharacterCard;
