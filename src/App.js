import React from "react";
import "./App.css";
import CharacterList from "./components/characterList/CharacterList";
import CharacterPage from "./components/characterPage/CharacterPage";
import Navbar from "react-bootstrap/Navbar";
import "bootstrap/dist/css/bootstrap.min.css";

import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar bg="dark">
          <Navbar.Brand>
            <Link to="/">Rick and Morty</Link>
          </Navbar.Brand>
        </Navbar>
        <Switch>
          <Route path="/character" component={CharacterPage}></Route>
          <Route path="/">
            <CharacterList />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
